package com.weslleymendonca.desafioconcrete.interfaces;

import com.weslleymendonca.desafioconcrete.model.PullRequest;
import com.weslleymendonca.desafioconcrete.model.RespostaRepositorio;
import com.weslleymendonca.desafioconcrete.model.Usuarios;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by weslleymendonca on 31/10/17.
 */

public interface GitServices {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RespostaRepositorio> getRepositories(@Query("page") Integer page);

    @GET("repos/{user}/{repo}/pulls")
    Call<List<PullRequest>>getPullRequests(@Path("user") String userName, @Path("repo") String repoName);

    @GET("users/{user}")
    Call<Usuarios> getUser(@Path("user") String username);
}
