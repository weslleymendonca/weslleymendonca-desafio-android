package com.weslleymendonca.desafioconcrete.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.weslleymendonca.desafioconcrete.R;
import com.weslleymendonca.desafioconcrete.adapter.PullAdapter;
import com.weslleymendonca.desafioconcrete.interfaces.GitServices;
import com.weslleymendonca.desafioconcrete.model.PullRequest;
import com.weslleymendonca.desafioconcrete.util.Api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullActivity extends AppCompatActivity {

    private String nomeAutor;
    private String nomeRepo;
    private String date;
    private RecyclerView listPullRequest;
    private PullAdapter adapter;
    private GitServices gitHubService;
    private List<PullRequest> pullRequests;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        nomeAutor = getIntent().getStringExtra("authorName");
        nomeRepo  = getIntent().getStringExtra("repoName");

        if (nomeAutor.isEmpty() || nomeRepo.isEmpty()) {
            Toast.makeText(this, "Ops... algo saiu errado! ", Toast.LENGTH_SHORT).show();
            finish();
        }

        getSupportActionBar().setTitle(nomeRepo);

        progressBar     = (ProgressBar) findViewById(R.id.pullProgressBar);
        gitHubService   = Api.getGitHubService();
        listPullRequest = (RecyclerView) findViewById(R.id.pullList);
        adapter = new PullAdapter(this, new ArrayList<PullRequest>(0),
                new PullAdapter.PullRequestListener() {
                    @Override
                    public void onPullRequestClick(String url) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    }
                });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listPullRequest.setLayoutManager(layoutManager);
        listPullRequest.setAdapter(adapter);
        listPullRequest.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        listPullRequest.addItemDecoration(itemDecoration);

        if (savedInstanceState != null) {
            progressBar.setVisibility(View.GONE);
            pullRequests = (List<PullRequest>) savedInstanceState.getSerializable("list");
            adapter.updatePullRequest(pullRequests);
        } else {
            loadPullRequests();
        }
    }

    public void loadPullRequests() {
        gitHubService.getPullRequests(nomeAutor, nomeRepo).enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    pullRequests = response.body();
                    adapter.updatePullRequest(pullRequests);
                    Log.i("PullRequestActivity", "Pull requests carregados.");
                } else {
                    // TODO: handle request errors
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Ops... Algo saiu errado!", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                Log.i("PullRequestActivity", "Erro ao carregar a API!");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("list", (Serializable) pullRequests);
    }
}
