package com.weslleymendonca.desafioconcrete.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by weslleymendonca on 31/10/17.
 */

public class RespostaRepositorio {

    @SerializedName("items")
    @Expose
    private List<Repositorio> repositories = null;
    @SerializedName("total_count")
    @Expose
    private Integer numRepositories;

    public List<Repositorio> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repositorio> repositories) {
        this.repositories = repositories;
    }

    public Integer getNumRepositories() {
        return numRepositories;
    }

    public void setNumRepositories(Integer numRepositories) {
        this.numRepositories = numRepositories;
    }
}
