package com.weslleymendonca.desafioconcrete.util;

import com.weslleymendonca.desafioconcrete.interfaces.GitServices;

/**
 * Created by weslleymendonca on 31/10/17.
 */

public class Api {

    public static final String BASE_URL = "https://api.github.com";

    public static GitServices getGitHubService() {

        return RetrofitClient.getClient(BASE_URL).create(GitServices.class);
    }
}
