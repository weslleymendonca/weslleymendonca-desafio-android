package com.weslleymendonca.desafioconcrete;

import com.weslleymendonca.desafioconcrete.util.Api;

import org.junit.Test;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by weslleymendonca on 31/10/17.
 */

public class ApiTest {


    @Test
    public void testGitHubServiceNotNull(){

        Api api = new Api();
        //assertNull (api.getGitHubService());
        assertNotNull(api.getGitHubService());
    }
}
